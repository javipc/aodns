# Actualizador Open DNS

### [0.7] 27 de Julio de 2019
Agregado: Compatibilidad con Ripple (https://guardianproject.info/apps/info.guardianproject.ripple/)
Agregado: Pantalla de Opciones.

### [0.1] - 10 de Mayo de 2019
Versión Inicial.

