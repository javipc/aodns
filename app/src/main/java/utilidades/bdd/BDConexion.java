package utilidades.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import utilidades.MensajeRegistro;
import utilidades.bdd.Tabla;

/**
 * Javier 18/11/2016.
 * conexión a la base de datos
 */
public class BDConexion extends SQLiteOpenHelper {


    // conexión a la base de datos de la aplicación
    private Tabla tablas[];

    public BDConexion(Context context, Tabla[] tablas) {
        super(context, "BaseDeDatos", null, 1);
        this.tablas = tablas;
        mensajeLog("<<<< constructor >>>");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // sqLiteDatabase.execSQL(BDRegistro.TABLA);
        mensajeLog(" on create !!!!!!!!!!");
        crearTablas(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        mensajeLog(" on update !!!!!!!!!!!!!!!!!!!!! ");
    }




    // ------------------------


    private void crearTablas (SQLiteDatabase sqLiteDatabase) {
        mensajeLog("crear tablassss -> a continuación ejecutará script");
        for (Tabla tabla : tablas)
            crearTabla(sqLiteDatabase, tabla);
    }

    private void crearTabla (SQLiteDatabase sqLiteDatabase, Tabla tabla) {
        String comando = obtenerComandoCrear(tabla);
        mensajeLog("************  script:    " +  comando);
        sqLiteDatabase.execSQL(comando);
    }



    // ----------------------- creador de script

    private String obtenerComandoCrear (Tabla tabla) {
        return "Create Table if not exists "  + tabla.getNombre() + " (" + obtenerScriptTablas(tabla) + ");";
    }

    private String obtenerScriptTablas (Tabla tabla) {
        String tablaString = "";
        for (Columna columna: tabla.getColumnas()) {

                if (!tablaString.isEmpty())
                    tablaString = tablaString + ", ";
                tablaString = tablaString
                        + columna.getNombre()
                        + " "
                        + interpretarTipo(columna.getTipo());
            }

            return tablaString;
    }



    private String interpretarTipo (Columna.Tipo tipo) {
        switch (tipo) {
            case texto: return "text";
            case entero: return "int";
            case indiceAutomatico: return "integer primary key autoincrement";
        }
        return "text";
    }

    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }

}
