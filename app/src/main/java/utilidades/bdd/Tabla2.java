package utilidades.bdd;


import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Javier 2019.
 */

public class Tabla2 {
    private ArrayList<Columna> tabla = new ArrayList<Columna>();
    private int contador = 0;
    private String nombre;

    public Tabla2(String nombre) {
        this.nombre = nombre;
    }

    protected void agregar (String nombre, Tipo tipo) {
        agregar(nombre, tipo, contador);
        contador = contador +1;
    }

    protected void agregar (String nombre, Tipo tipo, int indice) {
        Columna columna = new Columna();
        columna.setNombre(nombre);
        columna.setTipo(tipo.string());
        columna.setIndice(indice);
        tabla.add(columna);
    }


    public int indice (String nombre) {
        for (Columna c : tabla)
            if (c.getNombre().equals(nombre))
                return c.getIndice();
        return -1;
    }

    public String nombre (int indice) {
        for (Columna c : tabla)
            if (c.getIndice() == indice)
                return c.getNombre();
        return "";
    }


    public String getNombre () { return  nombre; }


    public String sqlString () {

        StringBuilder tablas = new StringBuilder();
        for (Columna c : tabla) {
            if (tablas.length() > 0)
                tablas.append(", ");

            tablas.append(c.getNombre()).append(" ").append(c.getTipo());
        }


        return "Create Table if not exists "  + nombre  + " (" + tablas + ");";
    }




    private class Columna {
        private String nombre = "id";
        private String tipo = "int";
        private int indice = 0;

        String getNombre() {
            return nombre;
        }

        void setNombre(String nombre) {
            this.nombre = nombre;
        }

        String getTipo() {
            return tipo;
        }

        void setTipo(String tipo) {
            this.tipo = tipo;
        }

        int getIndice() {
            return indice;
        }

        void setIndice(int indice) {
            this.indice = indice;
        }
    }




    public enum Tipo {
        texto ("text"),
        entero ("int"),
        indiceAutomatico ("integer primary key autoincrement");

        private final String t;

        Tipo (String tipo) {
            this.t = tipo;
        }

        public String string() {
            return t;
        }
    }


    private void xxx () {
        Field propiedades[] = (this.getClass()).getDeclaredFields();
        for (Field x :
                propiedades) {
            System.out.println (x.getName());
            System.out.println (x.getType());
        }
    }

}
