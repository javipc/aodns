package utilidades.bdd;

/**
 * Javier 2019.
 */




public class Columna {
    private String nombre = "id";
    private Tipo tipo = Tipo.texto;
    private int indice = 0;

    public Columna(Tipo tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString () { return getNombre(); }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public String getTipoString() {
        return tipo.string();
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }


    public enum Tipo {
        texto("text"),
        entero("int"),
        indiceAutomatico("integer primary key autoincrement");

        private final String t;

        Tipo(String tipo) {
            this.t = tipo;
        }

        public String string() {
            return t;
        }
    }

}
