package utilidades.bdd;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.Field;

import utilidades.MensajeRegistro;


/**
 * Javier 2019.
 *  Posee columnas que componen una tabla
 *  Posee metodos comunes para simplificar las clases que manejan bases de datos
 */

public abstract class Tabla {

    // nombre de la clase columna, sirve para identificarlas, contarlas, etc
    private final String tipoColumna = Columna.class.getSimpleName();

    // nombre de la tabla
    private String nombre;

    // columna identificadora
    private String columnaIdentificadora = "id";

    // almacena las columnas
    private Columna columnas[];



    public Tabla(String nombre) {
        this.nombre = nombre;

        // esto no - debe procesarse desde la clase heredada
        // procesar();

    }

    public String getNombre () { return nombre; }

    public Columna[] getColumnas () {
        if (columnas == null)
            procesar();
        return columnas;
    }


    public int cantidadDeColumnas () {
        int contador = 0;
        for (Field x : this.getClass().getDeclaredFields()) {
            if (!x.getType().getSimpleName().equals(tipoColumna))
                continue;
            contador++;
        }
        return contador;
    }





    /*
        Detecta las columnas presentes en la clase heredada
        Las modifica para un manejo más simple
            Agrega el nombre de la comuna
            Agrega el número de orden de la columna
            Encuentra la columna índice
     */
    protected void procesar () {
        int total = cantidadDeColumnas();
        columnas = new Columna[total];

        int contador = 0;

        for (Field x : this.getClass().getDeclaredFields()) {
            if (!x.getType().getSimpleName().equals(tipoColumna))
                continue;


            // obtiene las propiedades de esta clase columnas (que son propiedades de "Tabla")
            try {
                x.setAccessible(true);
                mensajeLog("this: " + x.get(this).toString());
                mensajeLog("getName: " + x.getName());


                // obtiene el objeto columna
                Columna columna = (Columna) x.get(this);

                // personaliza los parametros
                columna.setNombre(x.getName());
                columna.setIndice(contador);

                // agrega a la lista
                columnas[contador] = columna;

                // incrementa el contador para mantener el indice y orden de columnas
                contador++;

                // establece el nombre de la columna indice
                if (columna.getTipo().equals(Columna.Tipo.indiceAutomatico))
                    columnaIdentificadora = columna.getNombre();

            } catch (Exception e) { e.printStackTrace(); }
        }

    }
















    // ------------------------------------

    private SQLiteDatabase sql;

    public Tabla (String nombre, SQLiteDatabase sqLiteDatabase) {
        this.nombre = nombre;
        this.sql = sqLiteDatabase;
    }

    public void setSql (SQLiteDatabase sqLiteDatabase) {
        sql = sqLiteDatabase;
    }


    // ------------



    protected boolean insertar(String parametro, int valor) {
        ContentValues valores = new ContentValues();
        valores.put(parametro, valor);
        return  insertar(valores);
    }

    protected boolean insertar(String parametro, String valor) {
        ContentValues valores = new ContentValues();
        valores.put(parametro, valor);
        return  insertar(valores);
    }

    protected boolean insertar(ContentValues valores) {
        if (sql == null)
            mensajeLog("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ SQL NULO !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        int respuesta = (int) sql.insert(nombre,null, valores);
        valores.clear();
        return  (respuesta > 0);
    }





    protected void actualizar (int id, Columna columna, String valor) {
        actualizar(id, columna.getNombre(), valor);
    }

    protected void actualizar (int id, Columna columna, int valor) {
        actualizar(id, columna.getNombre(), valor);
    }

    protected void actualizar (int id, String parametro, String valor) {
        ContentValues valores = new ContentValues();
        valores.put(parametro, valor);
        actualizar(id, valores);
    }

    protected void actualizar (int id, String parametro, int valor) {
        ContentValues valores = new ContentValues();
        valores.put(parametro, valor);
        actualizar(id, valores);
    }




    protected void actualizar (int id, ContentValues valores) {
        sql.update(nombre, valores,columnaIdentificadora + " = " + id, null);
        mensajeLog ("Tupla Actualizada  " +columnaIdentificadora + " = " + id);
        valores.clear();
    }

    public boolean eliminar (int id) {
        return (sql.delete(nombre,columnaIdentificadora + " = " + id, null) > 0);
    }

    //elimina todo
    public boolean eliminar () {
        return (sql.delete(nombre,null, null) > 0);
    }






    protected Cursor seleccionar (int cantidad) {
        //Cursor c = sql.rawQuery("*",null);
        //Cursor c= sql.query(nombre,new String[]{"*"},null,null,null,null, null);
        Cursor c= sql.query(nombre,new String[]{"*"},null,null,null,null,null,String.valueOf(cantidad));
        mensajeLog("total cursor: " + c.getCount());
        return c;
    }

    protected Cursor seleccionar () {
        //Cursor c = sql.rawQuery("*",null);
        //Cursor c= sql.query(nombre,new String[]{"*"},null,null,null,null, null);
        Cursor c= sql.query(nombre,new String[]{"*"},null,null,null,null,null);
        return c;
    }



    protected Cursor seleccionarUltimos (int cantidad) {
        Cursor c= sql.query(nombre,new String[]{"*"},"",null,null,null,columnaIdentificadora + " desc", String.valueOf(cantidad));
        return c;
    }

    protected Cursor seleccionarUltimo () {
        return seleccionarUltimos(1);
    }




    public long contar () {
        return contar (null);
    }

    public long contar (String columna) {
        return DatabaseUtils.queryNumEntries(sql, nombre, columna);

    }


    // ---------------------------------------



    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }

}
