package utilidades.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.Field;

import utilidades.MensajeRegistro;

/**
 * Javier 2019.
 */

public abstract class BDAdaptador {

    private BDConexion conexion;
    protected SQLiteDatabase sqLiteDatabase;


    // almacena las tablas que componen la base de datos
    private Tabla tablas[];


    public BDAdaptador (Context contexto) {
        mensajeLog(">>> constructor");
        mensajeLog("-> instanciar tablas");
        instanciarTablas();
        mensajeLog("-> detectaaTablas");
        detectarTablas ();
        mensajeLog("->creando conexion");
        conexion = new BDConexion(contexto, tablas);
        abrir();

        mensajeLog("<<< constructor finalizado");
    }



    // Base de datos  -----------------------------------------



    public void abrir () {
        sqLiteDatabase = conexion.getWritableDatabase();
        asignarSqlATablas();
    }

    public void cerrar () {
        sqLiteDatabase.close();
    }



    // iniciar cada uno de los manejadores de la base de datos
    // bdCoordenada = new BDCoordenada();
    protected abstract void instanciarTablas () ;


    // asignar a cada uno de los manejadores el objeto sql
    // BDCoordenada.setSql (sql);

    protected void asignarSqlATablas() {
        for (Tabla tabla: tablas ) {
            tabla.setSql(sqLiteDatabase);
            mensajeLog("SQL ASIGNADO " + tabla.getNombre());
        }


    }





    protected void detectarTablas () {
        mensajeLog("detectarTablas: ");
        // nombre de la clase buscada
        String nombreDeClase = Tabla.class.getSimpleName();

        // total de clases
        int total = 0;
        for (Field x : this.getClass().getDeclaredFields())
            if (x.getType().getSuperclass().getSimpleName().equals(nombreDeClase))
                total++;

        tablas = new Tabla[total];

        mensajeLog("Total de tablas = " + total);


        int contador = 0;

        for (Field x : this.getClass().getDeclaredFields()) {
            if (!x.getType().getSuperclass().getSimpleName().equals(nombreDeClase))
                continue;


            // obtiene las propiedades de esta clase (tablas)
            try {
                mensajeLog(" TABLA DETECTADA: " + x.getName());
                // obtiene el objeto tabla
                x.setAccessible(true);
                Tabla tabla = (Tabla) x.get(this);

                // agrega a la lista
                tablas[contador] = tabla;

                // incrementa el contador para mantener el indice
                contador++;


            } catch (Exception e) { e.printStackTrace(); }
        }

    }










    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }

}
