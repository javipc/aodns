package pc.javier.actualizadoropendns;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import pc.javier.actualizadoropendns.adaptador.Constante;
import utilidades.EnlaceExterno;


public class ActividadAyuda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ayuda);
        TextView version = (TextView) findViewById(R.id.version);
        if (version != null)
            version.setText(Constante.version);

    }



    public void donar(View v) {
        EnlaceExterno enlaceExterno = new EnlaceExterno(this);
        enlaceExterno.abrirEnlace(Constante.urlDonacion);
    }

    public void sitio (View v) {
        EnlaceExterno enlaceExterno = new EnlaceExterno(this);
        enlaceExterno.abrirEnlace(Constante.urlSitio);
    }



}
