package pc.javier.actualizadoropendns.adaptador;

import android.content.Context;

import pc.javier.actualizadoropendns.control.receptor.ReceptorComandosInternet;
import utilidades.ConexionHTTP;
import utilidades.Evento;
import utilidades.FechaHora;

/**
 * Javier 2019.
 * envia datos al Servidor
 * las respuesta las recibe el objeto Evento
 */

public class Servidor {

    private String usuario;
    private String clave;
    private String perfil = "HOME";
    private Evento evento;




    public Servidor (Context contexto) {
        Preferencias preferencias = new Preferencias(contexto);

        clave = preferencias.getClave();
        perfil = preferencias.getPerfil();
        usuario = preferencias.getUsuario();
        // información extra ------------
    }







    public void enviar () {
        ConexionHTTP conexion = new ConexionHTTP(Constante.urlODNS, evento);
        conexion.agregarParametro("hostname", perfil);
        conexion.setAutorizacion("Basic " + conexion.codificarBase64(usuario + ":" + clave));
        conexion.setUserAgent(Constante.userAgent);
        conexion.setSsl(true);
        conexion.ejecutarHilo();
    }



// ----------




    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }


    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }



}
