package pc.javier.actualizadoropendns.adaptador;

import pc.javier.actualizadoropendns.BuildConfig;

/**
 * Javier 2019.
 *  configuraciones y parámetros constantes
 */

public final class Constante {
    public final static String version = BuildConfig.VERSION_NAME;
    public final static String fechaVersion = "Junio 2019";

    public final static String userAgent = "actualzador opendns";
    public final static String urlDonacion = "http://javim.000webhostapp.com/donacion/";

    public final static String urlSitio = "http://javim.000webhostapp.com/aodns";

    public final static String urlODNS = "https://updates.opendns.com/nic/update";


}
