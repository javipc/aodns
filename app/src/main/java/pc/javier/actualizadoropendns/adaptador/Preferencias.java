package pc.javier.actualizadoropendns.adaptador;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Javier 2019.
 * Manejador de preferencias
 * almacena y recupera preferencias de la aplicación
 */

public class Preferencias extends utilidades.Preferencias {


    public Preferencias(Context contexto) {
        super(contexto);
    }

    /*public Preferencias(Context contexto, String nombrePreferencias) {
        super(contexto, nombrePreferencias);
    }*/



    public void guardar(TipoPreferencia clave, String valor) {
        guardar(clave.string(), valor);
    }

    public void guardar(TipoPreferencia clave, int valor) {
        guardar(clave.string(), valor);
    }

    public void guardar(TipoPreferencia clave, boolean valor) {
        guardar(clave.string(), valor);
    }

    public void guardarLong(TipoPreferencia clave, long valor) {
        guardarLong(clave.string(), valor);
    }


    public int obtenerInt(TipoPreferencia clave) {
        return obtenerInt(clave.string());
    }

    public String obtenerString(TipoPreferencia clave) {
        return obtenerString(clave.string());
    }

    public boolean obtenerBoolean(TipoPreferencia clave) {
        return obtenerBoolean(clave.string());
    }

    public long obtenerLong(TipoPreferencia clave) {
        return obtenerLong(clave.string());
    }


    public void borrar(TipoPreferencia clave) {
        borrar(clave.string());
    }






    public enum TipoPreferencia {


        usuario("usuario"),
        clave("clave"),
        perfil("perfil"),

        iniciarConSistema("iniciarConSistema"),
        configuracionInicial("configuracionInicial"),

        servicioActivo("activo"),
        menuInicial("menuInicial"),
        versionRegistrada("versionRegistrada"),

        presentacionInicial("presentacionInicial"),
        ultimaActualizacion("ultimaActualizacion"),



        activacionExterna ("activacionExterna"),
        modoAutomatico ("modoAutomatico"),


        ;


        private final String preferencia;

        TipoPreferencia(String preferencia) {
            this.preferencia = preferencia;
        }

        public String string() {
            return preferencia;
        }


    }














    public String getClave() { return obtenerString (TipoPreferencia.clave); }

    public void setClave(String clave) { guardar (TipoPreferencia.clave, clave); }

    public String getUsuario() { return obtenerString (TipoPreferencia.usuario); }

    public void setUsuario(String usuario) { guardar (TipoPreferencia.usuario, usuario); }


    public String getPerfil () {
        String respuesta = obtenerString (TipoPreferencia.perfil);
        if (respuesta.isEmpty())
            return "HOME";
        if (respuesta.equals(""))
            return "HOME";
        return respuesta;
    }

    public void setPerfil(String perfil) { guardar (TipoPreferencia.perfil, perfil); }


    public boolean getIniciarConSistema() { return obtenerBoolean (TipoPreferencia.iniciarConSistema); }

    public void setIniciarConSistema(boolean iniciarConSistema) { guardar (TipoPreferencia.iniciarConSistema, iniciarConSistema); }

    public boolean getConfiguracionInicial() { return obtenerBoolean (TipoPreferencia.configuracionInicial); }

    public void setConfiguracionInicial(boolean configuracionInicial) { guardar (TipoPreferencia.configuracionInicial, configuracionInicial); }

    public boolean getServicioActivo() { return obtenerBoolean (TipoPreferencia.servicioActivo); }

    public void setServicioActivo(boolean servicioActivo) { guardar (TipoPreferencia.servicioActivo, servicioActivo); }


    public boolean getMenuInicial() { return obtenerBoolean (TipoPreferencia.menuInicial); }

    public void setMenuInicial(boolean menuInicial) { guardar (TipoPreferencia.menuInicial, menuInicial); }

    public boolean getVersionRegistrada() { return obtenerBoolean (TipoPreferencia.versionRegistrada); }

    public void setVersionRegistrada(boolean versionRegistrada) { guardar (TipoPreferencia.versionRegistrada, versionRegistrada); }


    public boolean getPresentacionInicial() { return obtenerBoolean (TipoPreferencia.presentacionInicial); }

    public void setPresentacionInicial(boolean presentacionInicial) { guardar (TipoPreferencia.presentacionInicial, presentacionInicial); }


    public String getUltimaActualizacion () { return obtenerString (TipoPreferencia.ultimaActualizacion); }

    public void setultimaActualizacion (String ultimaActualizacion) { guardar (TipoPreferencia.ultimaActualizacion, ultimaActualizacion); }





    public boolean getActivacionExterna () { return obtenerBoolean(TipoPreferencia.activacionExterna);  }
    public void setActivacionExterna (boolean valor) { guardar(TipoPreferencia.activacionExterna, valor); }




    public boolean getModoAutomatico () { return obtenerBoolean(TipoPreferencia.modoAutomatico);  }
    public void setModoAutomatico (boolean valor) { guardar(TipoPreferencia.modoAutomatico, valor); }






}
