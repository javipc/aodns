package pc.javier.actualizadoropendns;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.control.ControlPantallaOpciones;
import pc.javier.actualizadoropendns.vista.PantallaOpciones;

/**
 * Created by Javier on 27/7/2019.
 */

public class ActividadOpciones extends AppCompatActivity {

    ControlPantallaOpciones control;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opciones);

        PantallaOpciones pantalla = new PantallaOpciones(this);
        Preferencias preferencias = new Preferencias(this);

        control = new ControlPantallaOpciones(pantalla, preferencias);
    }





    public void cancelar (View v) {
        control.cancelar ();
    }


    public void guardar (View v) {
        control.guardar();
    }




}
