package pc.javier.actualizadoropendns;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import pc.javier.actualizadoropendns.adaptador.Constante;
import pc.javier.actualizadoropendns.adaptador.Menu;
import pc.javier.actualizadoropendns.control.Aplicacion;
import pc.javier.actualizadoropendns.control.ControlPantallaPrincipal;
import utilidades.EnlaceExterno;

public class MainActivity extends AppCompatActivity {

    private Menu menu;
    private ControlPantallaPrincipal control;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menu = new Menu(this);
        control = new ControlPantallaPrincipal(this);
        Aplicacion.actividadPrincipal = this;
    }

    @Override
    public void onResume () {
        super.onResume();
        control.ultimaActualizacion();

    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU)
            menu.alternar ();
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (menu.abierto())
            menu.cerrar();
        else
            super.onBackPressed();
    }





    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        menu.alternar();
        return true;
    }






    public void activarClick (View v) {

        control.actualizar();

    }






    // Menú lateral -----------------------------------------------------------------

    public void menuClick (MenuItem item) {
        menu.cerrar();

        switch (item.getItemId()) {
            case R.id.menu_sesion:
                control.iniciarActividad(ActividadSesion.class);
                break;

            case R.id.menu_ayuda:
                control.iniciarActividad(ActividadAyuda.class);
                break;

            case R.id.menu_registros:
                control.iniciarActividad(ActividadRegistro.class);
                break;

            case R.id.menu_opciones:
                control.iniciarActividad(ActividadOpciones.class);
                break;

            case R.id.menu_donar:
                donar();
                break;
        }
    }

    public void menuAbrirClick (View v) {
        menu.alternar();
    }


    private void donar (){
        (new EnlaceExterno(this)).abrirEnlace(Constante.urlDonacion);
    }






}
