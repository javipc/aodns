package pc.javier.actualizadoropendns.bd;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

import utilidades.MensajeRegistro;
import utilidades.bdd.Columna;
import utilidades.bdd.Tabla;

/**
 * Javier 2019.
 */

public class BDRegistro extends Tabla {
    private Columna id = new Columna(Columna.Tipo.indiceAutomatico);
    private Columna fecha = new Columna(Columna.Tipo.texto);
    private Columna informacion = new Columna(Columna.Tipo.texto);
    private Columna error  = new Columna(Columna.Tipo.entero);



    public BDRegistro() {
        super("registro");
        mensajeLog("constructor -> procesar");
        procesar();
    }



    public boolean  insertar (String fechaString, boolean errorBoolean) {
        int errorInt = 0;
        if (errorBoolean)
            errorInt = 1;
        return insertar(fechaString, errorInt);
    }


    public boolean  insertar (String fechaString, String informacionString, int errorInt) {
        ContentValues valores = new ContentValues();
        valores.put (fecha.getNombre(), fechaString);
        valores.put (error.getNombre(), errorInt);
        valores.put (informacion.getNombre(), informacionString);
        return insertar(valores);
    }




    public Registro ultimo () {


        Cursor c = super.seleccionarUltimo();
        c.moveToFirst();

        if (c.getCount()==0)
            return null;

        return crearRegistro(c);
    }

    public ArrayList<Registro> todos () {
        ArrayList<Registro> lista = new ArrayList<>();
        Cursor cursor = seleccionar();

        int total = cursor.getCount();

        cursor.moveToFirst();

        if (total > 0)
            while (cursor.isAfterLast() == false) {

                Registro r = crearRegistro (cursor);
                lista.add(r);
                cursor.moveToNext();

            } ;

        mensajeLog("TOTAL=" +total);
        return lista;

    }

    private Registro crearRegistro (Cursor cursor) {
        Registro r = new Registro();
        r.fecha = cursor.getString(fecha.getIndice());
        r.error = cursor.getInt(error.getIndice());
        r.informacion = cursor.getString(informacion.getIndice());
        return r;
    }

    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }


    public class Registro {
        public String fecha;
        public int error;
        public String informacion;
    }
}
