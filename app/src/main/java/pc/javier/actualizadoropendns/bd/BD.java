package pc.javier.actualizadoropendns.bd;

import android.content.Context;

import java.util.ArrayList;

import utilidades.MensajeRegistro;
import utilidades.bdd.BDAdaptador;

/**
 * Javier 2019.
 */

public class BD extends BDAdaptador {

    public BD(Context contexto) {
        super(contexto);
    }

    @Override
    public void instanciarTablas() {
        mensajeLog("-> new bdregistro");
        bdRegistro = new BDRegistro();
        mensajeLog("<<< tablas instanciadas");
    }



    private BDRegistro bdRegistro;






    // --------------

    public void registroAgregar (String fecha, String informacion, int error) {
        bdRegistro.insertar (fecha, informacion, error);
    }


    public BDRegistro.Registro registroUltimo () {
        return bdRegistro.ultimo();
    }

    public ArrayList<BDRegistro.Registro> registroTodos () {
        return bdRegistro.todos();
    }

    public long registroContar () {
        return bdRegistro.contar();
    }


    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }


}
