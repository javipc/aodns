package pc.javier.actualizadoropendns.control.panico;

import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.adaptador.Servidor;
import pc.javier.actualizadoropendns.control.EnlaceEventos;

/**
 * Created by Javier on 27/7/2019.
 */

public class Panico extends utilidades.Panico {

    @Override
    public void activar () {
        Preferencias preferencias = new Preferencias(this);
        if (preferencias.getActivacionExterna() == false) {
            finish();
            return;
        }

        preferencias.setModoAutomatico(true);


        EnlaceEventos enlaceEventos = new EnlaceEventos(this);
        Servidor servidor = new Servidor(this);
        servidor.setEvento(enlaceEventos.obtenerEventoConexionServidor());
        servidor.enviar();



    }

}
