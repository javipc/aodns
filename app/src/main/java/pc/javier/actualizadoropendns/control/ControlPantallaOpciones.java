package pc.javier.actualizadoropendns.control;

import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.vista.PantallaOpciones;
import utilidades.MensajeRegistro;


/**
 * Javier 2019.
 */

public class ControlPantallaOpciones extends Control {


    private PantallaOpciones pantalla;


    public ControlPantallaOpciones(PantallaOpciones pantalla, Preferencias preferencias) {
        super(pantalla, preferencias);

        this.pantalla = pantalla;

        cargarOpciones();
    }




    public void guardar () {
        guardarOpciones();
        getPantalla().finalizarActividad();
    }


    public void cancelar () {
        getPantalla().finalizarActividad();
    }





    private void cargarOpciones() {
        pantalla.setActivacionExterna(preferencias.getActivacionExterna());
        pantalla.setModoAutomatico(preferencias.getModoAutomatico());

    }





    private void guardarOpciones () {

        preferencias.setModoAutomatico(pantalla.getModoAutomatico());
        preferencias.setActivacionExterna(pantalla.getActivacionExterna());
    }



    private void mensajeLog (String texto) {
        MensajeRegistro.msj (this, texto);
    }
}
