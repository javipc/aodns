package pc.javier.actualizadoropendns.control;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;
import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.bd.BD;
import pc.javier.actualizadoropendns.bd.BDRegistro;
import pc.javier.actualizadoropendns.vista.PantallaRegistros;
import utilidades.FechaHora;
import utilidades.MensajeRegistro;

/**
 * Javier 2019.
 */

public class ControlPantallaRegistros extends Control {

    private Context contexto;
    private PantallaRegistros pantalla;

    public ControlPantallaRegistros(Activity activity) {
        super(activity);

        contexto = activity;
        this.preferencias = new Preferencias(activity);
        this.pantalla = new PantallaRegistros(activity);

        cargarVista();

    }





    public void agregarRegistro () {
        BD bd = new BD(activity);
        bd.registroAgregar("hola", "chau", 1);

    }



    // carga el list view
    private void cargarVista() {
        ArrayList<BDRegistro.Registro> listaElementos = obtenerElementos();
        AdaptadorRegistro adaptador = new AdaptadorRegistro(contexto, listaElementos);
        pantalla.lista().setAdapter(adaptador);
    }


    // carga una lista de datos (registros) que servira para el list view
// obtiene coordenadas de la base de datos
    private ArrayList<BDRegistro.Registro> obtenerElementos() {
        // base de datos
        BD bd = new BD(contexto);

        // obtiene las coordenadas de la base de datos
        ArrayList<BDRegistro.Registro> listaElementos = bd.registroTodos();

        bd.cerrar();

        // inicialmente
        // mueve la marca a la posicion conocida

        mensajeLog("total elementos " + listaElementos.size());

        return listaElementos;
    }


    private class AdaptadorRegistro extends ArrayAdapter {
        private ArrayList<BDRegistro.Registro> listaElementos;

        public AdaptadorRegistro(Context context, ArrayList<BDRegistro.Registro> listaElementos) {
            super(context, R.layout.registrositem, listaElementos);
            this.listaElementos = listaElementos;
        }

        @Override
        public View getView(int posicion, View convertView, ViewGroup parent) {

            View elemento = pantalla.getActivity().getLayoutInflater().inflate(R.layout.registrositem, null);

            Pantalla e = new Pantalla(elemento);

            BDRegistro.Registro r = listaElementos.get(posicion);

            e.setTextView(R.id.registros_fechahora, r.fecha);
            e.setTextView(R.id.registros_informacion, r.informacion);
            if (r.error == 1) {
                //e.setTextView(R.id.registros_estado, "Error");
                elemento.setBackgroundColor(pantalla.colorRojo());
            }

            return elemento;
        }

        private String string(double valor) {
            return String.valueOf(valor);
        }

    }


    private void mensajeLog(String texto) {
        MensajeRegistro.msj(this, texto);
    }


}
