package pc.javier.actualizadoropendns.control;

import android.app.Activity;
import android.content.Context;



import pc.javier.actualizadoropendns.control.receptor.ReceptorComandosInternet;
import pc.javier.actualizadoropendns.control.receptor.ReceptorConexionInternet;
import utilidades.ConexionHTTP;
import utilidades.Evento;


/**
 * Javier 2019.
 *  une emisores de eventos con receptores de eventos
 */

public class EnlaceEventos {

    private Activity activity;
    private Context contexto;
    private Evento evento;

    public EnlaceEventos (Context contexto) {
        this.contexto = contexto;
        activity = Aplicacion.actividadPrincipal;

    }

    public EnlaceEventos (Activity activity) {
        this.contexto = activity.getApplicationContext();
        this.activity = activity;
    }

    //public EnlaceEventos () { activity = Aplicacion.actividadPrincipal; contexto = activity; }








    public Evento obtenerEventoConexionServidor () {
        String claveEstado = ConexionHTTP.claveEstado;
        String claveRespuesta = ConexionHTTP.claveRespuesta;
        evento = new Evento();
        agregarReceptorConexion(claveEstado);
        agregarReceptorComandos(claveRespuesta);
        return evento;
    }







    private void agregarReceptorComandos (String claveEventos) {
        ReceptorComandosInternet receptorComandosInternet = new ReceptorComandosInternet(contexto, claveEventos);
        evento.agregarReceptor(receptorComandosInternet);
    }


    private void agregarReceptorConexion (String claveEventos) {
        ReceptorConexionInternet receptorConexionInternet = new ReceptorConexionInternet(activity, claveEventos);
        evento.agregarReceptor(receptorConexionInternet);
    }







}
