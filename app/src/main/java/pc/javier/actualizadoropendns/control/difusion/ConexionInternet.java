package pc.javier.actualizadoropendns.control.difusion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.adaptador.Servidor;
import pc.javier.actualizadoropendns.control.EnlaceEventos;
import utilidades.MensajeRegistro;

/**
 * Javier 2019.
 */

public class ConexionInternet extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager conexion = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);


        final NetworkInfo infoRedes = conexion.getActiveNetworkInfo();


        MensajeRegistro.msj("RECEPTOR - cambio de internet");

        if (infoRedes == null)
            return;

        MensajeRegistro.msj ("Exste una conexión");



        Preferencias preferencias = new Preferencias(context);
        if (preferencias.getUsuario().isEmpty())
            return;

        if (preferencias.getModoAutomatico() == false)
            return;


        EnlaceEventos enlaceEventos = new EnlaceEventos(context);
        Servidor servidor = new Servidor(context);
        servidor.setEvento(enlaceEventos.obtenerEventoConexionServidor());
        servidor.enviar();

    }


}
