package pc.javier.actualizadoropendns.control;

import android.app.Activity;
import android.view.MenuItem;

import pc.javier.actualizadoropendns.ActividadSesion;
import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.adaptador.Servidor;
import pc.javier.actualizadoropendns.vista.PantallaPrincipal;
import utilidades.Dialogo;
import utilidades.EnlaceExterno;
import utilidades.MensajeRegistro;

/**
 * Javier 2019.
 */

public class ControlPantallaPrincipal  extends Control {

    private PantallaPrincipal pantalla;
    private Preferencias preferencias;
    public ControlPantallaPrincipal(Activity actividad) {
        super(actividad);
        pantalla = new PantallaPrincipal(actividad);
        preferencias = new Preferencias(actividad);
    }




    public void ultimaActualizacion () {
        if (preferencias.getUltimaActualizacion().isEmpty())
            pantalla.setTextView(R.id.ultimaActualizacion, "");
        else
            pantalla.setTextView(R.id.ultimaActualizacion, "Última Actualización: " + preferencias.getUltimaActualizacion());
    }




    public void actualizar () {
        if (preferencias.getUsuario().isEmpty()) {
            iniciarActividad(ActividadSesion.class);
            return;
        }

        EnlaceEventos enlaceEventos = new EnlaceEventos(activity);
        Servidor servidor = new Servidor(activity);
        servidor.setEvento(enlaceEventos.obtenerEventoConexionServidor());
        servidor.enviar();
    }



    // menú


    public void click (MenuItem opcion) {


        String usuario = getPreferencias().obtenerString(Preferencias.TipoPreferencia.usuario);
        boolean versionregistrada = getPreferencias().obtenerBoolean(Preferencias.TipoPreferencia.versionRegistrada);


        switch (opcion.getItemId()) {



        }
    }















}
