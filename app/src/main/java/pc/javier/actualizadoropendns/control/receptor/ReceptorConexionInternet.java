package pc.javier.actualizadoropendns.control.receptor;


import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;
import pc.javier.actualizadoropendns.bd.BD;
import pc.javier.actualizadoropendns.control.Aplicacion;
import pc.javier.actualizadoropendns.vista.PantallaPrincipal;
import utilidades.ConexionHTTP;
import utilidades.FechaHora;


/**
 * Javier 2019.
 *  recibe estados de la conexion a internet
 */

public class ReceptorConexionInternet extends utilidades.ReceptorEventos {

    private PantallaPrincipal pantallaPrincipal;
    private Activity actividad;

    public ReceptorConexionInternet(Activity activity, String clave) {
        super(clave);
        pantallaPrincipal = new PantallaPrincipal(Aplicacion.actividadPrincipal);
        actividad = activity;
    }


    @Override
    public void procesar(String dato) {

        if (dato.equals(ConexionHTTP.Estado.Conectando.name())) {
            pantallaPrincipal.snack(R.string.conectando);
            pantallaPrincipal.dibujarBoton(false);
        }


        if (dato.equals(ConexionHTTP.Estado.Finalizado.name())) {
            pantallaPrincipal.dibujarBoton(true);
        }

        if (dato.equals(ConexionHTTP.Estado.Error.name())) {
            pantallaPrincipal.snack(R.string.errorconexion);
            pantallaPrincipal.dibujarBoton(true);
            FechaHora fechaHora = new FechaHora();
            BD bd = new BD(actividad);
            bd.registroAgregar(fechaHora.obtenerHoraFechaNormal(),  pantallaPrincipal.getActivity().getString(R.string.errorconexion), 1);
            bd.cerrar();
        }

        if (dato.equals(ConexionHTTP.Estado.Recibiendo.name())) {

        }

        if (dato.equals(ConexionHTTP.Estado.Enviando.name())) {

        }

    }


}
