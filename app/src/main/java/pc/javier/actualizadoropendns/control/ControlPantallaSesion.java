package pc.javier.actualizadoropendns.control;

import android.app.Activity;
import android.util.Log;
import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Constante;
import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.adaptador.Servidor;
import pc.javier.actualizadoropendns.vista.PantallaSesion;

/**
 * Javier 2019.
 */

public class ControlPantallaSesion extends Control {

    private PantallaSesion pantalla;


    public ControlPantallaSesion(Activity activity) {
        super(activity);
        this.pantalla = new PantallaSesion(activity);
    }





    // carga y muestra las opciones guardadas en la pantalla de configuracion
    public void  cargarOpciones () {
        String perfil = preferencias.getPerfil();
        String usuario = preferencias.getUsuario();

        pantalla.setPerfil(perfil);
        pantalla.setUsuario(usuario);

    }


    private void guardarOpciones () {
        String perfil = pantalla.getPerfil().trim();
        String usuario = pantalla.getUsuario().trim();
        String clave = pantalla.getClave();



        preferencias.setPerfil(perfil);
        preferencias.setUsuario(usuario);
        preferencias.setClave(clave);

        //editor.putBoolean("ssl", ssl);
        //editor.putBoolean("sesion", true);


    }




    public void conectar () {


        String clave = pantalla.getClave();
        String usuario = pantalla.getUsuario().trim();
        String perfil = pantalla.getPerfil().trim();



        if (clave.length() == 0) {
            mostrarMensaje((R.string.sesion_clavecorta));
            return;
        }

        if (usuario.length() == 0) {
            mostrarMensaje((R.string.sesion_usuariocorto));
            return;
        }



        guardarOpciones();


        // conectar(usuario, clave, perfil);

        pantalla.finalizarActividad();

    }







    private void mostrarMensaje (int id) {
        pantalla.snack(id);
    }



    public void cancelar () {
        pantalla.finalizarActividad();
    }

    private void mensajeLog (String texto) {
        Log.d("Control pantalla Sesion", texto);
    }
}
