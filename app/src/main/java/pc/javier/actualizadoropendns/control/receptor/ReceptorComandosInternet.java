package pc.javier.actualizadoropendns.control.receptor;


import android.app.Activity;
import android.content.Context;

import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;
import pc.javier.actualizadoropendns.adaptador.Preferencias;
import pc.javier.actualizadoropendns.bd.BD;
import pc.javier.actualizadoropendns.control.Aplicacion;
import utilidades.FechaHora;
import utilidades.MensajeRegistro;
import utilidades.ReceptorEventos;

/**
 * Javier 2019.
 *  Recibe comandos por parte del Servidor
 */

public class ReceptorComandosInternet extends ReceptorEventos{
    private Preferencias preferencias;
    //private Activity activity;
    private Pantalla pantallaPrincipal;
    private Context contexto;


    public ReceptorComandosInternet(Context contexto, String clave) {
        super(clave);
        //this.activity = activity;
        this.contexto = contexto;
        preferencias = new Preferencias(contexto);
        pantallaPrincipal = new Pantalla(Aplicacion.actividadPrincipal);

    }

    @Override
    public void procesar(String dato) {
        MensajeRegistro.msj("receptor COMANDOS - " +dato);

        // al dato lo "subdivide" en comando y parametro
        int indice = dato.indexOf(" ");
        String comando = dato;
        String parametro = "";

        if (indice > 0) {
            comando = dato.substring(0, indice);
            parametro = dato.substring(indice + 1);
        }

        ejecutar (comando, parametro);
    }





    private void ejecutar (String comando, String parametro) {

        int error = 0;
        switch (comando) {

            case "badauth":
                pantallaPrincipal.snack(R.string.claveIncorrecta);
                error = 1;
                break;
            case "nohost":
                pantallaPrincipal.snack(R.string.perfilIncorrecto);
                error = 1;
                break;
            case "good":
            case "!yours":
                pantallaPrincipal.snack(R.string.actualizacionCorrecta + " " + parametro);
                pantallaPrincipal.setTextView(R.id.ultimaActualizacion, "Ahora");
                FechaHora fechaHora = new FechaHora();
                preferencias.setultimaActualizacion(fechaHora.obtenerFechaHoraNormal());
                break;
            case "abuse":
                pantallaPrincipal.snack(R.string.bloqueado);
                error = 1;
                break;

        }
        FechaHora fechaHora = new FechaHora();
        BD bd = new BD(contexto);
        bd.registroAgregar(fechaHora.obtenerHoraFechaNormal(), comando + " " + parametro, error);
        bd.cerrar();
    }




    // ------------------------------------------- COMANDOS ----






    // -----------------------------------------------------

    private int  entero (String numero) {
        try { return Integer.parseInt(numero); }
            catch (Exception e) { return 0; }
    }

    private boolean boleano (String valor) {
        try { return Boolean.parseBoolean(valor); }
        catch (Exception e) { return false; }
    }






    private void mensajeLog (String texto) { MensajeRegistro.msj("RECEPTOR COMANDOS", texto);}
}
