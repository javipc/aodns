package pc.javier.actualizadoropendns;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import pc.javier.actualizadoropendns.control.ControlPantallaSesion;


public class ActividadSesion extends AppCompatActivity {


    ControlPantallaSesion control;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sesion);

        control = new ControlPantallaSesion(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        control.cargarOpciones();
    }





    public void conectar (View v) {
        control.conectar ();
    }

    public void cancelar (View v) {
        control.cancelar();
    }





}
