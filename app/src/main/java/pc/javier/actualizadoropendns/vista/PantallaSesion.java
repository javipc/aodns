package pc.javier.actualizadoropendns.vista;

import android.app.Activity;
import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;


/**
 * Javier 2019.
 * maneja la pantalla de sesión
 */

public class PantallaSesion extends Pantalla {

    private Activity activity;
    public PantallaSesion(Activity activity) {
        super(activity);
        this.activity = activity;
    }





    public void setUsuario (String texto) {
        setEditText(R.id.sesion_usuario, texto);
    }
    public String getUsuario () {
        return getEditText(R.id.sesion_usuario).getText().toString();
    }

    public String getClave () {
        return getEditText(R.id.sesion_clave).getText().toString();
    }


    public void setPerfil (String texto) {
        setEditText(R.id.sesion_perfil, texto);
    }
    public String getPerfil () {
        return getEditText(R.id.sesion_perfil).getText().toString();
    }


    public void setEstado (String texto) { setTextView(R.id.sesion_estado, texto); }
    public void setEstado (int idString) { setTextView(R.id.sesion_estado, idString); }




    public void habiltarBotonIniciar (boolean habilitado) {
        if (getButton(R.id.sesion_boton_iniciar) == null)
            return;

        getButton(R.id.sesion_boton_iniciar).setEnabled(habilitado);
        if (habilitado)
            setFondo(R.id.sesion_boton_iniciar, cuadroVerdeRedondeando());
        else
            setFondo(R.id.sesion_boton_iniciar, cuadroGrisRedondeando());
    }


}
