package pc.javier.actualizadoropendns.vista;

import android.app.Activity;
import android.widget.ListView;

import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;



/**
 * Javier 2019.
 */

public class PantallaRegistros extends Pantalla {
    public PantallaRegistros(Activity activity) {
        super(activity);
    }



    public ListView lista () {
        return (ListView) getView(R.id.registros_lista);
    }


}
