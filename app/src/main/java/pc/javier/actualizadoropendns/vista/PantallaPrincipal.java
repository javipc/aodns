package pc.javier.actualizadoropendns.vista;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.util.Size;
import android.view.animation.AnimationUtils;

import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;
import utilidades.EditorVistas;
import utilidades.Efecto;

/**
 * Javier 2019.
 */

public class PantallaPrincipal extends Pantalla {
    public PantallaPrincipal(Activity activity) {
        super(activity);
    }




    public void dibujarBoton (boolean activado) {

        int idBoton = R.id.princ_boton;


        if (getButton(idBoton) == null)
            return;

        int botonActivado = 0;
        int botonDesactivado = 0;


        botonActivado = R.drawable.anilloverde;
        botonDesactivado =R.drawable.botonprincipalredondogris;



        if (activado) {
            setButtonText(idBoton, R.string.actualizar);
            getButton(idBoton).setBackgroundResource(botonActivado);
            getButton(idBoton).setTextColor(Color.BLACK);
            getButton(idBoton).setTextSize(30);
            getButton(idBoton).setEnabled(true);

        } else {
            setButtonText(idBoton, R.string.espere);
            getButton(idBoton).setTextColor(Color.DKGRAY);
            getButton(idBoton).setBackgroundResource(botonDesactivado);
            getButton(idBoton).setTextSize(15);
            getButton(idBoton).setEnabled(false);
        }



        getButton(idBoton).clearAnimation();
        getButton(idBoton).startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_atras_entrada));


    }











    private void mostrarIcono (int idIcono, boolean activo) {
        if (getImageView(idIcono).getVisibility() == visibilidad(activo))
            return;
        Efecto.AnimarIcono(getImageView(),  activo);
    }



    /* public void usuario (String usuario) {
        setTextView(R.id.menu_texto_usuario, usuario);
    }*/



}
