package pc.javier.actualizadoropendns.vista;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;


import pc.javier.actualizadoropendns.R;
import pc.javier.actualizadoropendns.adaptador.Pantalla;
import utilidades.EditorVistas;

/**
 * Javier 2019.
 */

public class PantallaOpciones extends Pantalla {
    public PantallaOpciones(Activity activity) {
        super(activity);
    }



    public boolean getActivacionExterna () { return getToggle(R.id.opciones_permitir_activacion_externa).isChecked();  }
    public void setActivacionExterna (boolean valor) { setToggle(R.id.opciones_permitir_activacion_externa, valor); }


    public boolean getModoAutomatico () { return getToggle(R.id.opciones_modo_automatico).isChecked();  }
    public void setModoAutomatico (boolean valor) { setToggle(R.id.opciones_modo_automatico, valor); }








}
