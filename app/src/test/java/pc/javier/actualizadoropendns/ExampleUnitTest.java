package pc.javier.actualizadoropendns;

import android.util.Base64;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Arrays;

import utilidades.FechaHora;
import utilidades.Reflexion;
import utilidades.bdd.Columna;

import pc.javier.actualizadoropendns.bd.BDRegistro;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }



    @Test
    public void clave () {
        String basic = "Basic " + new String(Base64.encode("hola:chau".getBytes(),Base64.NO_WRAP ));
        System.out.println(basic);


    }




    @Test
    public void bd () {
        Columna c = new Columna(Columna.Tipo.entero);

        System.out.println(c.getNombre());

        for (Field x : c.getClass().getDeclaredFields()) {

            if (x.getName().equals("this$0"))
                continue;

            System.out.println(">>>" + x.getName());

            System.out.println("---" + x.getType().getSimpleName());

            // obtiene las propiedades de esta clase columnas (que son propiedades de "Esquema")
            try {
                if (x.getName().equals("nombre"))
                    x.set (c, "noooooooo");
            } catch (Exception e) {
                e.printStackTrace();
            }




            System.out.println(c.getNombre());
        }




    }


































    @Test
    public void demo () {
        tt t = new tt(4);

        t.proc(Hola.values());
    }



    public class tt {

        Columna columnas[];
        int total = 0;
        public tt (int t) {
            columnas = new Columna[t];
        }


        public void proc (Object s[]) {

            for (Object r : s) {
                System.out.println("<>" + r.toString());
            }

        }

    }


    public enum Hola {
        nombre, apellido, direccion,
    }




















    @Test
    public void dedladjealid() {
        BDRegistro j = new BDRegistro();

    }



    @Test
    public void u () {
        Columna c1 = new Columna(Columna.Tipo.texto);
        Columna c2 = new Columna(Columna.Tipo.texto);
        Columna c3 = new Columna(Columna.Tipo.texto);

        Columna columnas[] = new Columna[5];


        arr (new String[] { "hola", "chau" });

        System.out.println(Columna.class.getSimpleName());
        boolean o = true;

    }

    public void arr (String[] x) {

    }





    @Test
    public void ref () {
        System.out.println(FechaHora.class.getName());
        Reflexion reflexion = new Reflexion("utilidades.FechaHora");
        FechaHora f = (FechaHora) reflexion.obtenerObjeto();
        System.out.println(f.obtenerHoraFechaNormal());
    }

}